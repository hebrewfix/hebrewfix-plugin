# Changelog

changes documentation


## [1.6] - 2020-08-15
- Restored /hf command

## [1.5] - 2018-02-03
- Removed /hf command

- Added support for custom commands

- Changed config.yml to hold command templates

- Fixed different client languages communication on commands



## [1.4] - 2015-09-15
- Sign fixes -Default enabler depending on game language. -For "CraftBukkit 1.8.8"
- Version 1.4 of hebrewFix. :) Enjoy!


## [1.3] - 2014-07-20
- Anyone can disable the plugin for themeselves (and the choice is saved for always)

- Mixed sentences (for example: hebrew and english and numbers on the same sentence)

- For CraftBukkit 1.7.9R0.1 (might work on older versions)

- Version 1.3 of hebrewFix. :) Enjoy!

## [1.2]
- Unreleased version

## [1.1] - 2013-08-31
- Improved efficency. Version 1.1 of hebrewFix. :) Enjoy!


## [1.0] - 2013-08-31
- The first release. :) Enjoy!
