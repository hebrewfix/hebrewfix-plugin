## Our compiled libs

includes:
- CraftBukkit
- Spiggot

## How to compile

Download BuildTools.jar from here:

https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar

Open cmd go to BuildTools directory and run:
```
java -jar BuildTools.jar
```