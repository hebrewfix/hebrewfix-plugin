This is the copyright notice for the HebrewFix plugin.
According to the license of the source code stated in LICENSE.md, this copyright notice should be included in each re-distribution of the software.
You have been warned.

-----------------------------------------

The rights are reserved for g_o, PvPNiK a.k.a HebrewFixTeam for the project and it's name.
