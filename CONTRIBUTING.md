Contribution Guidelines
----------------------------

To contribute please follow the next guidelines to help your code get in the repo:

1. Do not upload any eclipse/other IDE files (e.g: .project, .classpath, .settings, etc...)
2. We only accept git contributions via pull requests. MAILING CODE WILL BE IGNORED.
3. Documentation:
	* You must document your code in the java standards.
	* Look at other examples in the code to figure out how.
	* Functions must be documented with the multi-line comments: "/\* \*/"
	* Complicated logic must be documented with single line comment: "// "
4. Your code must be able to be:
	* Built into a JAR file.
	* Running without errors on a bukkit server. (1.8+)
5. Support in discord: https://discord.gg/enrucwU
6. Thank you!