## Minecraft hebrewfix plugin

Bukkit plugin page:
https://dev.bukkit.org/projects/languagefix?gameCategorySlug=bukkit-plugins&projectID=64686

CurseForge page:
https://www.curseforge.com/minecraft/bukkit-plugins/languagefix

Spigot page:
https://www.spigotmc.org/resources/hebrewfix.52764/

## Contribution

To contribute, check out CONTRIBUTING.md first. We only accept pull requests.

## Support

Available in Discord: https://discord.gg/enrucwU

## Eclipse setup

Right click -> New -> Java Project

Select project from git and complete project creation.

Right click on project -> Properties -> Java Build Path -> Add JARs

Add libs/craftbukkit-1.12.2.jar.

## Testing guide

In eclipse - right click on project -> Export -> Java/Jar. Select all src folder and on the right select config.yml and plugins.yml only.

Server creation - Recommended: use bukkitGUI from here - https://dev.bukkit.org/projects/bukkitgui/files/920757

Create directory "test-server" (already in gitignore)

Move bukkitgui / jar to there. Configure bukkitgui to use spiggot/bukkit from libs.

Create directory test-server/plugins and copy 

Run server.

## TODO

* Bugfix: opposite client language in same server bug.
* Feature: BungeeCord support
* Feature: custom language
* Git: incorporate open source methodology
