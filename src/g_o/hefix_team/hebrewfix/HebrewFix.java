package g_o.hefix_team.hebrewfix;

import java.util.logging.Logger;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * HebrewFix plugin - bukkit plugin for reversed rtl languages.
 * @author g_o, PvPNiK
 *
 */
public class HebrewFix extends JavaPlugin {
	final Logger logger = Logger.getLogger("Minecraft");	// plugin logger
	PluginDescriptionFile pdfFile = getDescription();		// plugin description

	/**
	 * Plugin enable method.
	 */
	@Override
	public void onEnable() {
		// Load config
		getConfig();
		saveDefaultConfig();
		// Update pmCmds
		PrivateMessageCommands.setTemplates(getConfig().getStringList("private-messages-commands"));
		// Set event manager
		getServer().getPluginManager().registerEvents(new EventManager(), this);
		// Set command executor
		this.getCommand("hf").setExecutor(new CommandKit());
		// Log
		logger.info(pdfFile.getName() + " V" + pdfFile.getVersion() + " Has Been Enabled");
	}

	/**
	 * Plugin disable method.
	 */
	@Override
	public void onDisable() {
		logger.info(pdfFile.getName() + " Has Been Disabled");
	}
}
