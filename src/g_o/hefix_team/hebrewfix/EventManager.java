package g_o.hefix_team.hebrewfix;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class EventManager implements Listener {	
	private static final int MAX_SIGN_LINES = 4;

	/**
	 * onPutSign - event handler for modifying/placing signs.
	 * @param e - the sign update event.
	 */
	@EventHandler
	public void onPutSign(SignChangeEvent e) {
		Player player = e.getPlayer();

		// Check if player is hebrew
		if (Utils.isHebrewClient(player) || Utils.isOptOut(player))
			return;
		
		// Fix line by line
		String[] lines = e.getLines(); 
		if (lines.length > MAX_SIGN_LINES)
			return;
		
		for (int i = 0; i < lines.length; i++)
			e.setLine(i, Utils.fixMessage(lines[i]));
	}

	/**
	 * onPrivateMessage - handler for intercepting private messages for fixing.
	 * @param e - the pm event.
	 */
	@EventHandler
	public void onPrivateMessage(PlayerCommandPreprocessEvent e) {
		String cmd = e.getMessage();
		if (!cmd.contains(" ")) return;									// Check if command has args
		if (!PrivateMessageCommands.isPrivateMessageCmd(cmd)) return;	// Check if private message
		if (e.isCancelled()) return;									// Check if event is cancelled
		boolean isHebClient = Utils.isHebrewClient(e.getPlayer());
		boolean isRecvHebClient = isHebClient;
		
		
		String cmdTemplate = PrivateMessageCommands.getTemplate(cmd);
		if (cmdTemplate == null) return;	// check template exists
		
		String[] cmdArgs = cmd.split(" ");

		// handle message argument
		int msgIndex = PrivateMessageCommands.getArgIndex(cmdTemplate, "message");
		if (msgIndex == -1 || msgIndex >= cmdArgs.length) return;	// if there's no message we don't have anything to fix?
		
		// handle player argument
		int nameIndex = PrivateMessageCommands.getArgIndex(cmdTemplate, "player");
		if (nameIndex != -1) {
			if (nameIndex  >= cmdArgs.length) return;	// Make sure inside template 
			Player recvPlayer = Bukkit.getPlayer(cmdArgs[nameIndex]);  
			if (recvPlayer == null) return;
			// check if 
			isRecvHebClient = Utils.isHebrewClient(recvPlayer); 
			if (isRecvHebClient && isHebClient) return;
		}

		// join pre-msg
		String preMsg = "";
		for (int i = 0; i < msgIndex; i++)
			preMsg += cmdArgs[i] + " ";
		
		// get msg splitted
		String msg = "";
		for (int i = msgIndex; i < cmdArgs.length; i++)
			msg += cmdArgs[i] + " ";

		// fix and send
		if (isHebClient == isRecvHebClient) // both english clients
			e.setMessage(preMsg + Utils.fixMessage(msg));
		else								// clients are incompatible
			e.setMessage(preMsg + Utils.fixMessageIncompatible(msg, isHebClient));
	}


	/**
	 * onPlayerChat - handling chat event
	 * @param e - chat event
	 */
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		String msg = e.getMessage();
		if (Utils.shouldFix(e.getPlayer(), msg))
			e.setMessage(Utils.fixMessage(msg));
	}
}
