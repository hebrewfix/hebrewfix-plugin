package g_o.hefix_team.hebrewfix;

import org.bukkit.ChatColor;

public class Strings {
	public static final String onOffMsg = "you've turned automatic hebrew fix";
	public static final String onMsg = "ON";
	public static final String offMsg = "OFF";

	/**
	 * getOnOffString - get apropriate message for opting in/out
	 * @param isout - true for opt out; false for opt in
	 * @return String - the message
	 */
	public static String getOnOffString(boolean isout)
	{
		String state = "";
		if (isout)
			state = ChatColor.RED + offMsg + ChatColor.RESET;
		else
			state = ChatColor.GREEN + onMsg + ChatColor.RESET;

		return "[" + onOffMsg + " " + state + "]";
	}
}