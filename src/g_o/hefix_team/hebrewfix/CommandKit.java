package g_o.hefix_team.hebrewfix;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandKit implements CommandExecutor {

	/**
	 * onCommand - handling command event
	 * @param sender - 			the command sender
	 * @param cmd - 			the command
	 * @param commandLabel - 	the command label
	 * @param args - 			the command arguments
	 *
	 * @return boolean - true if sucessful
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(commandLabel.equalsIgnoreCase("hf")){ // Toggle plugin
			Player player = (Player) sender;
			boolean newOptState = !Utils.isOptOut(player);

			// opt in/out and reply
			Utils.optPlayer(player, newOptState);
			sender.sendMessage(Strings.getOnOffString(newOptState));
		}

		return true;
	}
}
