package g_o.hefix_team.hebrewfix;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.entity.Player;

public class Utils {

	public static Set<UUID> optOutPlayers = new HashSet<UUID>();

	/**
	 * isOptOut - checks if a player has opted out from hebrewfix
	 * @param p - the player instance
	 * @return true if opted out; false otherwise
	 */
	public static boolean isOptOut(Player p)
	{
		return optOutPlayers.contains(p.getUniqueId());
	}

	/**
	 * opt in/out a player
	 * @param p - the player instance
	 * @param isout - true for opt out; false for opt in
	 */
	public static void optPlayer(Player p, boolean isout)
	{
		if (isout == isOptOut(p))
			return;

		UUID playerUUID = p.getUniqueId();

		if (isout)
			optOutPlayers.add(playerUUID);
		else
			optOutPlayers.remove(playerUUID);
	}

	/**
	 * shouldFix - wether this message needs fixing for player
	 * @param p - the player
	 * @param msg - the message to potentially fix
	 * @return boolean - true: should; false: shouldn't
	 */
	public static boolean shouldFix(Player p, String msg) {
		return (isHebrew(msg) && (!isHebrewClient(p))) && !Utils.isOptOut(p);
	}
	
	/**
	 * Fix an array of messages
	 * @param msgs - the messages to fix & concatenate
	 * @return String - result of the operation
	 */
	public static String fixAll(String[] msgs) {
		String result = "";
		for (String msg : msgs) {
			result += fixMessage(msg);
		}
		
		return result;
	}
	
	/**
	 * Fix Hebrew reversed message if needed to.
	 * @param msg - the message to fix.
	 * @return String - the fixed message. 
	 */
	public static String fixMessage(String msg) {
		if (!isHebrew(msg)) return msg;
		return fixFromWordsArray(msg.split(" "));
	}
	
	/**
	 * Fix message for incompatible clients
	 * @param msg - the message to fix
	 * @param isSenderHeb - is sender client in hebrew
	 * @return String - the fixed message
	 */
	public static String fixMessageIncompatible(String msg, boolean isSenderHeb) {
		if (!isHebrew(msg)) return msg;	// Check messag is in hebrew
		String[] words = msg.split(" ");
		
		if (isSenderHeb)
			return fixFromWordsArray(words);
		else
			return fixWordsIncompatible(words);
	}
	
	/**
	 * isHebrew - a function that retursn true if a String has hebrew letters in it.
	 * @author: nikita (AKA PvPNiK)
	 * @param str - the string to check if it have a hebrew letter in it.
	 * @return true if it has hebrew letters in it, otherwise false.
	 */
	public static boolean isHebrew(String str) {
		if (str == null) return false;
		Pattern p = Pattern.compile("\\p{InHebrew}");
		Matcher m = p.matcher(str);
		return m.find();
	}
	
	/**
	 * Gets player local using reflection.
	 * @param p - player
	 * @return String - the player locale.
	 */
    public static String getPlayerLocale(Player p){
        try {
                Object ep = p.getClass().getDeclaredMethod("getHandle").invoke(p);
                if(ep != null) {
	                Field f = ep.getClass().getDeclaredField("locale");
	                f.setAccessible(true);
	                return (String) f.get(ep);
                } else
                	return "";
        } catch (Exception e) {e.printStackTrace();}
        return "";
    }
	
	/**
	 * isHebrewClient - a function that returns true if a player is playing on hebrew.
	 * @author: nikita (AKA PvPNiK)
	 * @param p - the player we are checking.
	 * @return true if 'p' playing on hebrew.
	 */
	public static boolean isHebrewClient(Player p) {
		return getPlayerLocale(p).toLowerCase().startsWith("he");
	}
	
	/**
	 * Reverse a string
	 * @param s - the string to revese
	 * @return String - the reversed string
	 */
	public static String reverseString(String s) {
		String result = "";
		for (int i = s.length()-1; i > -1; i--)
			result += s.charAt(i);
		return result;
	}
	
	/**
	 * fixFromWordsArray - a function that reverse hebrew letters (avoiding non hebrew letters).
	 * @param words - the words to reverse.
	 * @return String - the fixed reversed string.
	 */
	public static String fixFromWordsArray(String[] words) {
		String result = "";
		for (int i = words.length - 1; i > -1; i--) {
			if (isHebrew(words[i])) {
				for (int j = words[i].length() - 1; j > -1; j--)
					result += words[i].charAt(j);
			} else {
				for (int j = 0; j < words[i].length(); j++)
					result += words[i].charAt(j);
			}
			result += " ";
		}
		return result.trim();
	}
	
	/**
	 * fixWordsIncomatible - fixFromWordsArray for incompatible clients
	 * Changes rtl <--> ltr
	 * @param words - the words of the message
	 * @return String - fixed string
	 */
	public static String fixWordsIncompatible(String[] words) {
		String result = "";
		for (int i = words.length - 1; i > -1; i--) {
			result += words[i] + " ";
		}
		return result.trim();
	}
}
